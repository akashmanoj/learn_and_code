package football;

import java.util.Scanner;

public class FootballChallenge {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		FootballMatch match = new FootballMatch();
		int numberOfMatches;
		numberOfMatches = Integer.parseInt(s.next());
		int lastPlayer;
		
		for(int i = 0; i < numberOfMatches; i++) {
			lastPlayer = match.beginGame(s);
			System.out.println("Player " + lastPlayer);
		}
	}

}
