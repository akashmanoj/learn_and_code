package football;

import java.util.Scanner;

public class FootballMatch {

	//Scanner s = new Scanner(System.in);
	String nextMove;
	int nextPlayer;
	int numberOfMoves;
	boolean isContinous = false;
	int previousPlayer;
	
	int beginGame(Scanner s) {
		numberOfMoves = Integer.parseInt(s.next());
		Stack stack = new Stack(numberOfMoves + 1);
		stack.push(s.nextInt());
		for(int i = 0; i < numberOfMoves; i++) {
			nextMove = s.next();
			if(nextMove.equalsIgnoreCase("P")) {
				nextPlayer = s.nextInt();
				stack.push(nextPlayer);
				isContinous = false;
			}
			else if(nextMove.equalsIgnoreCase("B") && isContinous == false) {
				try {
					previousPlayer = stack.pop();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				isContinous = true;
			}
			else if((nextMove.equalsIgnoreCase("B") && isContinous == true)) {
				stack.push(previousPlayer);
				isContinous = false;
			}
		}
		return stack.peek();
	}

}
