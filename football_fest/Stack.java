package football;

public class Stack {
	int stack[];
	int top = -1;

	public Stack(int size) {
		stack = new int[size];
	}
	
	void push(int n){
		if(top == stack.length) {
			throw new ArrayIndexOutOfBoundsException();
		}
		stack[++top] = n;
	}
	
	int pop() throws Exception{
		if(top < 0) {
			throw new ArrayIndexOutOfBoundsException();
		}
		return stack[top--];
	}
	
	int peek(){
		if(top < 0) {
			throw new ArrayIndexOutOfBoundsException();
		}
		return stack[top];
	}
}
