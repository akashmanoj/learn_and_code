package little_monk;

public class Stack {
	private int top = -1;
	private int stack[];
	private int sizeOfStack;
	
	public Stack(int sizeOfStack) {
		this.sizeOfStack = sizeOfStack;
		stack = new int[sizeOfStack];
	}
	
	void push(int valueToPush) {
   	    	 if(top == sizeOfStack - 1) {
   		     	throw new IndexOutOfBoundsException("Overflow Exception");
    		}
      			stack[++top] = valueToPush;
  	  }
	
	int pop() throws Exception{
		if(top < 0) {
			throw new ArrayIndexOutOfBoundsException();
		}
		return stack[top--];
	}
		
	boolean isStackEmpty(){
		if(top < 0) {
			return true;
		}
		return false;
	}
	
	void emptyTheStack(){
		top = -1;
	}
	
	boolean isStackEmpty(){
		if(top < 0) {
			return true;
		}
		return false;
	}
	
	void emptyTheStack(){
		top = -1;
	}
	
	int peek(){
		if(top < 0) {
			throw new ArrayIndexOutOfBoundsException();
		}
		return stack[top];
	}
}
