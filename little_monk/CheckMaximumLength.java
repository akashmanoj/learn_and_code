package little_monk;

public class CheckMaximumLength {
	private int currentLength = 0;
	Stack stack;
	
	public CheckMaximumLength(int numberOfParameters) {
		stack = new Stack(numberOfParameters);
	}
	
	void closingParanthesis(int negativeNumber) throws Exception{		
        if(stack.isStackEmpty() == true) {
            currentLength = 0;
            return;
        }        
        if(isTopOfStackMatchingWith(negativeNumber))
        	closeOpenedParanthesis();
        else
        	makeStackEmpty();
    }
	
	void openingParanthesis(int positiveNumber) {
		stack.push(positiveNumber);
	}
	
	private boolean isTopOfStackMatchingWith(int negativeNumber) {
		if(stack.peek() + negativeNumber == 0)
			return true;
		else
			return false;
	}
	
	private void closeOpenedParanthesis() throws Exception {
		currentLength += 2;
        stack.pop();
        if(stack.isStackEmpty()) {
        	LittleMonk.maxLength += currentLength;
        	currentLength = 0;
        }
	}
	
	private void makeStackEmpty() {
		if(currentLength > LittleMonk.maxLength) 
    			LittleMonk.maxLength = currentLength;
        currentLength = 0;
        stack.emptyTheStack();
	}

	void checkIfEmpty(){
		if(stack.isStackEmpty() == false){
			if(currentLength > LittleMonk.maxLength) 
        		LittleMonk.maxLength = currentLength;
		}
	}
}
