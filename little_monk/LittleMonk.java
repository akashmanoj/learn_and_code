package little_monk;

import java.util.Scanner;

public class LittleMonk {
	public static int maxLength = 0;
	
	public static void main(String[] args) throws Exception {
		Scanner s = new Scanner(System.in);
		int numberOfParameters = s.nextInt();
		int value;
		CheckMaximumLength checkMaximumLength = new CheckMaximumLength(numberOfParameters);
		
		for (int i = 0; i < numberOfParameters; i++) {
            value = s.nextInt();
            
            if(value > 0){
            	checkMaximumLength.openingParanthesis(value);
            }
            else
                checkMaximumLength.closingParanthesis(value);
        }
		
		checkMaximumLength.checkIfEmpty();		
		System.out.println(maxLength);
		s.close();
	}

}
